#[cfg(test)]
mod t_var_statement {
  use tskit::{
    var::{
      VarDeclaration,
      VarStatement,
      VarDeclarationList,
    },
    Keyword,
    Text,
  };

  #[test]
  fn a01() {
    let mut decl_list = VarDeclarationList::new();
    let mut n_decl = VarDeclaration::new("n");

    n_decl.init(&Keyword::NaN);

    decl_list
      .declaration(n_decl)
      .let_mod();

    let mut actual = VarStatement::new(decl_list);

    actual.export();

    assert_eq!(actual.text(), "export let n = NaN");
  }

  #[test]
  fn a02() {
    let mut decl_list = VarDeclarationList::new();
    let mut n_decl = VarDeclaration::new("n");

    n_decl.init(&Keyword::NaN);

    decl_list
      .declaration(n_decl)
      .let_mod();

    let actual = VarStatement::new(decl_list);

    assert_eq!(actual.text(), "let n = NaN");
  }
}
