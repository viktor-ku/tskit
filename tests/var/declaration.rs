#[cfg(test)]
mod t_var_declaration {
  use tskit::{
    var::{
      VarDeclaration,
    },
    Keyword,
    Text,
    Node,
    Kind,
    UnionType,
    literal::NumericLiteral,
  };

  #[test]
  fn a01() {
    let var = VarDeclaration::new("foo");

    assert_eq!(var.kind(), Kind::VarDeclaration);
    assert_eq!(var.text(), "foo");
  }

  #[test]
  fn a02() {
    let mut var = VarDeclaration::new("doneYet");

    var.init(&Keyword::False);

    assert_eq!(var.text(), "doneYet = false");
  }

  #[test]
  fn a03() {
    let mut var = VarDeclaration::new("doneYet");

    var
      .init(&Keyword::False)
      .ts_type(&Keyword::Boolean);

    assert_eq!(var.text(), "doneYet: boolean = false");
  }

  #[test]
  fn a04() {
    let mut var = VarDeclaration::new("humanId");

    let mut union = UnionType::new();

    union
      .add(&Keyword::String)
      .add(&Keyword::Null);

    var
      .init(&Keyword::Null)
      .ts_type(&union);

    assert_eq!(var.text(), "humanId: string | null = null");
  }

  #[test]
  fn a05() {
    let mut var = VarDeclaration::new("age");
    let age: NumericLiteral = (30.1 - 7.1).into();

    var
      .init(&age)
      .ts_type(&Keyword::Number);

    assert_eq!(var.text(), "age: number = 23");
  }
}
