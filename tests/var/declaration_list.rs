#[cfg(test)]
mod t_var_declaration_list {
  use tskit::{
    var::{
      VarDeclarationList,
      VarDeclaration,
    },
    Keyword,
    Text,
    Node,
    Kind,
  };

  #[test]
  fn a01() {
    let actual = VarDeclarationList::new();
    assert_eq!(actual.kind(), Kind::VarDeclarationList);
  }

  #[test]
  fn a02() {
    let mut actual = VarDeclarationList::new();
    let me_decl = VarDeclaration::new("me");

    actual
      .declaration(me_decl)
      .let_mod();

    assert_eq!(actual.text(), "let me");
  }

  #[test]
  fn a03() {
    let mut actual = VarDeclarationList::new();
    let undef = Keyword::Undefined;

    let mut a_decl = VarDeclaration::new("a");
    let mut b_decl = VarDeclaration::new("b");

    a_decl.init(&undef);
    b_decl.init(&undef);

    actual
      .declaration(a_decl)
      .declaration(b_decl)
      .var_mod();

    assert_eq!(actual.text(), "var a = undefined, b = undefined");
  }
}
