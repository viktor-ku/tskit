use std::fs::File;
use std::io::prelude::*;

pub fn read_file(mocks: &str, filename: &str) -> std::io::Result<String> {
  let path = format!("tests/mocks/{}/{}.ts", mocks, filename);
  let mut file = File::open(path)?;
  let mut buff = String::with_capacity(100);
  file.read_to_string(&mut buff)?;
  buff.truncate(buff.len() - 1); // remove \n at the end of the file
  Ok(buff)
}

use tskit::{
  Text,
  var::{
    VarStatement,
    VarDeclaration,
    VarDeclarationList,
  },
};

pub fn make_var<'a>(name: &'a str, init: &'a Text) -> Box<VarStatement<'a>> {
  let mut decl = VarDeclaration::new(name);
  decl.init(init);

  let mut decl_list = VarDeclarationList::new();
  decl_list.declaration(decl);

  let var = VarStatement::new(decl_list);

  Box::new(var)
}
