mod common;

#[cfg(test)]
mod t_class_declaration {
  use super::common::{
    read_file,
    make_var,
  };

  use tskit::{
    var::{
      VarDeclarationList,
      VarDeclaration,
      VarStatement,
    },
    literal::{
      ObjectLiteralExpression,
      StringLiteral,
      NumericLiteral,
    },
    PropertyAssignment,
    MethodDeclaration,
    Keyword,
    Text,
    Node,
    Kind,
    Block,
    ReturnStatement,
    PropertyAccessExpression,
    Identifier,
    ClassDeclaration,
    PropertyDeclaration,
    Decorator,
    CallExpression,
    HeritageClause,
    Token,
    ExpressionWithTypeArguments,
  };

  #[test]
  fn a01() {
    let mut class = ClassDeclaration::new();
    class.name("Human");

    let expected = read_file("class_declaration", "a01").unwrap();
    assert_eq!(class.text(), expected);
  }

  #[test]
  fn a02() {
    let mut class = ClassDeclaration::new();
    class.name("Human");

    let john_str: StringLiteral = "John".into();
    let mut name_prop = PropertyDeclaration::new("name");
    name_prop.init(&john_str);

    class.member(&name_prop);

    let expected = read_file("class_declaration", "a02").unwrap();
    assert_eq!(class.text(), expected);
  }

  #[test]
  fn a03() {
    let mut class = ClassDeclaration::new();
    class.name("App");

    let mut name_prop = PropertyDeclaration::new("name");

    name_prop
      .ts_type(&Keyword::String)
      .modifier(&Keyword::Public);

    let mut api_prop = PropertyDeclaration::new("api");
    let zero: NumericLiteral = 0.into();

    api_prop
      .ts_type(&Keyword::Number)
      .modifier(&Keyword::Private)
      .init(&zero);

    let mut desc_prop = PropertyDeclaration::new("desc");

    desc_prop
      .question()
      .ts_type(&Keyword::String)
      .protected();

    let deco_func_name = Identifier::new("Deco");

    let mut call = CallExpression::new(&*deco_func_name);
    let obj = ObjectLiteralExpression::new();

    call
      .argument(&Keyword::Null)
      .argument(&obj);

    let decorator = Decorator::new(&call);

    class
      .decorator(&decorator)
      .member(&name_prop)
      .member(&api_prop)
      .member(&desc_prop);

    let expected = read_file("class_declaration", "a03").unwrap();
    assert_eq!(class.text(), expected);
  }

  #[test]
  fn a04() {
    let mut class = ClassDeclaration::new();
    class.name("Dog");

    let exp_with_arg = ExpressionWithTypeArguments::new("Animal");
    let extends = HeritageClause::new(Token::Extends, &exp_with_arg);

    let i_animal = ExpressionWithTypeArguments::new("IAnimal");
    let implements = HeritageClause::new(Token::Implements, &i_animal);

    class
      .heritage_clause(extends)
      .heritage_clause(implements);

    let expected = read_file("class_declaration", "a04").unwrap();
    assert_eq!(class.text(), expected);
  }
}
