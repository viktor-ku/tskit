#[cfg(test)]
mod t_import {
  use tskit::{
    import::{
      ImportDeclaration,
      ImportClause,
      ImportSpecifier,
      NamedImports,
      NamespaceImport,
    },
    Text,
  };

  #[test]
  fn a01() {
    let mut clause = ImportClause::new();

    let ns = NamespaceImport::new("yay");
    clause.named_bindings(&ns);

    let import = ImportDeclaration::new(clause, "nya");

    assert_eq!(import.text(), "import * as yay from 'nya'");
  }

  #[test]
  fn a02() {
    let mut clause = ImportClause::new();

    clause.name("Monster");

    let import = ImportDeclaration::new(clause, "nya");

    assert_eq!(import.text(), "import Monster from 'nya'");
  }

  #[test]
  fn a03() {
    let mut clause = ImportClause::new();

    let mut specifier = ImportSpecifier::new("Monster");
    specifier.prop_name("Monster001");

    let actor = ImportSpecifier::new("Actor");

    let mut named_imports = NamedImports::new(vec![specifier]);
    named_imports.element(actor);

    clause.named_bindings(&named_imports);

    let import = ImportDeclaration::new(clause, "nya");

    assert_eq!(import.text(), "import { Monster001 as Monster, Actor } from 'nya'");
  }

  #[test]
  fn a04() {
    let mut clause = ImportClause::new();

    let nu = ImportSpecifier::new("Nu");
    let named_imports = NamedImports::new(vec![nu]);

    clause
      .named_bindings(&named_imports)
      .name("Def");

    let import = ImportDeclaration::new(clause, "nya");

    assert_eq!(import.text(), "import Def, { Nu } from 'nya'");
  }
}
