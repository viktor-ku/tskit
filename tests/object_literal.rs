mod common;

#[cfg(test)]
mod t_object_literal {
  use super::common::{
    read_file,
    make_var,
  };

  use tskit::{
    var::{
      VarDeclarationList,
      VarDeclaration,
      VarStatement,
    },
    literal::{
      ObjectLiteralExpression,
      StringLiteral,
      NumericLiteral,
    },
    PropertyAssignment,
    MethodDeclaration,
    Keyword,
    Text,
    Node,
    Kind,
    Block,
    ReturnStatement,
    PropertyAccessExpression,
    Identifier,
  };

  #[test]
  fn a01() {
    let name: StringLiteral = "Viktor".into();
    let age: NumericLiteral = 23.into();

    let name_prop = PropertyAssignment::new("name", &name);
    let age_prop = PropertyAssignment::new("age", &age);

    let mut obj = ObjectLiteralExpression::new();

    obj
      .prop(&name_prop)
      .prop(&age_prop);

    let var = make_var("me", &obj);
    let expected = read_file("object_literal", "a01").unwrap();

    assert_eq!(var.text(), expected);
  }

  #[test]
  fn a02() {
    let name: StringLiteral = "Viktor".into();
    let name_prop = PropertyAssignment::new("name", &name);
    let mut get_name_method = MethodDeclaration::new("getName");
    let mut get_name_method_body = Block::new();
    let this = Keyword::This;
    let this_name = PropertyAccessExpression::new("name", &this);

    let ret = ReturnStatement::with_expression(&this_name);

    get_name_method_body.statement(&ret);
    get_name_method.body(get_name_method_body);

    let mut obj = ObjectLiteralExpression::new();

    obj
      .prop(&name_prop)
      .method(&get_name_method);

    let var = make_var("me", &obj);
    let expected = read_file("object_literal", "a02").unwrap();

    assert_eq!(var.text(), expected);
  }
}
