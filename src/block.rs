use crate::{
  Identifier,
  Text,
  Node,
  Kind,
  utils::StringBuilder,
};

#[derive(Default)]
pub struct Block<'a> {
  statements: Vec<&'a Text>,
}

impl<'a> Block<'a> {
  pub fn new() -> Self {
    Default::default()
  }

  pub fn with_statements(statements: Vec<&'a Text>) -> Self {
    Self {
      statements,
    }
  }

  pub fn statement(&mut self, statement: &'a Text) -> &mut Self {
    self.statements.push(statement);
    self
  }
}

impl<'a> Text for Block<'a> {
  fn text(&self) -> String {
    let mut sb = StringBuilder::with_capacity(100);

    sb.curly_brace_start();

    if !self.statements.is_empty() {
      sb.eol();

      for statement in &self.statements {
        sb
          .pad(2)
          .add(&statement.text())
          .eol();
      }

      sb.pad(1);
    }

    sb.curly_brace_end();

    sb.to_string()
  }
}

impl<'a> Node for Block<'a> {
  fn kind(&self) -> Kind {
    Kind::Block
  }
}
