use super::{
  Kind,
  Node,
  Text,
  utils::StringBuilder,
  Identifier,
};

pub struct ExpressionWithTypeArguments {
  expression: Box<Identifier>,
}

impl ExpressionWithTypeArguments {
  pub fn new(text: &str) -> Self {
    Self {
      expression: Identifier::new(text),
    }
  }
}

impl Text for ExpressionWithTypeArguments {
  fn text(&self) -> String {
    let mut sb = StringBuilder::with_capacity(40);

    sb.add(&self.expression.text());

    sb.to_string()
  }
}

impl Node for ExpressionWithTypeArguments {
  fn kind(&self) -> Kind {
    Kind::ExpressionWithTypeArguments
  }
}
