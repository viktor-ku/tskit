use crate::{
  Text,
  Node,
  Kind,
  utils::StringBuilder,
  Identifier,
};

pub struct PropertyAccessExpression<'a> {
  name: Box<Identifier>,
  expression: &'a Text,
}

impl<'a> PropertyAccessExpression<'a> {
  pub fn new(name: &str, expression: &'a Text) -> Self {
    Self {
      name: Identifier::new(name),
      expression,
    }
  }
}

impl<'a> Text for PropertyAccessExpression<'a> {
  fn text(&self) -> String {
    let mut sb = StringBuilder::with_capacity(100);

    sb
      .add(&self.expression.text())
      .dot()
      .add(&self.name.text());

    sb.to_string()
  }
}

impl<'a> Node for PropertyAccessExpression<'a> {
  fn kind(&self) -> Kind {
    Kind::PropertyAccessExpression
  }
}
