use super::{
  Text,
  Node,
  kind::Kind,
  constants::*,
};

/// Represent different tokens in TypeScript
pub enum Token {
  /// &&
  AmpAmp,

  /// *
  Asterisk,

  /// ||
  BarBar,

  /// ...
  DotDotDot,

  /// =>
  EqualsGreaterThan,

  /// !
  Exclamation,

  /// -
  Minus,

  /// +
  Plus,

  /// ?
  Question,

  /// implements
  Implements,

  /// extends
  Extends,

  EndOfFileToken,
}

impl Text for Token {
  fn text(&self) -> String {
    match self {
      Token::AmpAmp => AMP_AMP.to_string(),
      Token::Asterisk => ASTERISK.to_string(),
      Token::BarBar => BAR_BAR.to_string(),
      Token::DotDotDot => DOT_DOT_DOT.to_string(),
      Token::EqualsGreaterThan => EQUALS_GREATER_THAN.to_string(),
      Token::Exclamation => EXCLAMATION.to_string(),
      Token::Minus => MINUS.to_string(),
      Token::Plus => PLUS.to_string(),
      Token::Question => QUESTION.to_string(),
      Token::EndOfFileToken => String::new(),
      Token::Extends => EXTENDS.to_string(),
      Token::Implements => IMPLEMENTS.to_string(),
    }
  }
}

impl Node for Token {
  fn kind(&self) -> Kind {
    match self {
      Token::AmpAmp => Kind::AmpAmpToken,
      Token::Asterisk => Kind::AsteriskToken,
      Token::BarBar => Kind::BarBarToken,
      Token::DotDotDot => Kind::DotDotDotToken,
      Token::EqualsGreaterThan => Kind::EqualsGreaterThanToken,
      Token::Exclamation => Kind::ExclamationToken,
      Token::Minus => Kind::MinusToken,
      Token::Plus => Kind::PlusToken,
      Token::Question => Kind::QuestionToken,
      Token::EndOfFileToken => Kind::EndOfFileToken,
      Token::Extends => Kind::ExtendsToken,
      Token::Implements => Kind::ImplementsToken,
    }
  }
}
