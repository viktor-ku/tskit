use crate::{
  Text,
  Node,
  Kind,
  utils::StringBuilder,
  Identifier,
};

pub struct ExportSpecifier {
  name: Box<Identifier>,
  property_name: Option<Box<Identifier>>,
}

impl ExportSpecifier {
  /// Creates new ExportSpecifier
  ///
  /// ```rust
  /// use tskit::{ Kind, Node, Text };
  /// use tskit::export::ExportSpecifier;
  ///
  /// let simple_ex = ExportSpecifier::new("a");
  ///
  /// assert_eq!(simple_ex.text(), "a");
  /// assert_eq!(simple_ex.kind(), Kind::ExportSpecifier);
  /// ```
  pub fn new(name: &str) -> Self {
    Self {
      name: Identifier::new(name),
      property_name: None,
    }
  }

  /// Add propertyName identifier
  ///
  /// ```rust
  /// use tskit::{ Kind, Node, Text };
  /// use tskit::export::ExportSpecifier;
  ///
  /// let mut ex_as = ExportSpecifier::new("b");
  /// ex_as.prop_name("a");
  ///
  /// assert_eq!(ex_as.text(), "a as b");
  /// ```
  pub fn prop_name(&mut self, val: &str) -> &mut Self {
    self.property_name = Some(Identifier::new(val));
    self
  }
}

impl Text for ExportSpecifier {
  fn text(&self) -> String {
    let mut sb = StringBuilder::with_capacity(20);

    if let Some(id) = &self.property_name {
      sb.add(&id.text())
        .space()
        .add_as()
        .space();
    }

    sb.add(&self.name.text());

    sb.to_string()
  }
}

impl Node for ExportSpecifier {
  fn kind(&self) -> Kind {
    Kind::ExportSpecifier
  }
}
