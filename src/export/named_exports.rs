use crate::{
  Text,
  Node,
  Kind,
  utils::StringBuilder,
};

use super::{
  ExportSpecifier,
};

pub struct NamedExports {
  elements: Vec<ExportSpecifier>,
}

impl NamedExports {
  /// Creates new NamedExports
  ///
  /// ```rust
  /// use tskit::export::{ NamedExports, ExportSpecifier };
  /// use tskit::{ Text, Node, Kind };
  ///
  /// let elements = vec![ExportSpecifier::new("a"), ExportSpecifier::new("b")];
  /// let named_exports = NamedExports::new(elements);
  ///
  /// assert_eq!(named_exports.text(), "{ a, b }");
  /// assert_eq!(named_exports.kind(), Kind::NamedExports);
  /// ```
  pub fn new(elements: Vec<ExportSpecifier>) -> Self {
    Self {
      elements,
    }
  }

  /// Creates new NamedExports with given capacity in case we know exact number of exports in
  /// advance.
  ///
  /// ```rust
  /// use tskit::export::{ NamedExports, ExportSpecifier };
  /// use tskit::{ Text, Node };
  ///
  /// let elements = vec![ExportSpecifier::new("a"), ExportSpecifier::new("b")];
  /// let mut named_exports = NamedExports::with_capacity(elements.len());
  ///
  /// named_exports
  ///   .elements(elements);
  ///
  /// assert_eq!(named_exports.text(), "{ a, b }");
  /// ```
  pub fn with_capacity(capacity: usize) -> Self {
    Self {
      elements: Vec::with_capacity(capacity),
    }
  }

  /// Use to reset ExportSpecifiers vector
  pub fn elements(&mut self, val: Vec<ExportSpecifier>) -> &mut Self {
    self.elements = val;
    self
  }

  /// Use to add ExportSpecifier to the elements vector
  ///
  /// ```rust
  /// use tskit::export::{ NamedExports, ExportSpecifier };
  /// use tskit::{ Text, Node };
  ///
  /// let mut named_exports = NamedExports::with_capacity(2);
  ///
  /// named_exports
  ///   .element(ExportSpecifier::new("a"))
  ///   .element(ExportSpecifier::new("b"));
  ///
  /// assert_eq!(named_exports.text(), "{ a, b }");
  /// ```
  pub fn element(&mut self, val: ExportSpecifier) -> &mut Self {
    self.elements.push(val);
    self
  }
}

impl Text for NamedExports {
  fn text(&self) -> String {
    let mut sb = StringBuilder::with_capacity(40);

    sb.curly_brace_start()
      .space();

    for el in &self.elements {
      sb.add(&el.text())
        .comma()
        .space();
    }

    sb.remove_last(2) // removes space + comma from the last elements iteration
      .space()
      .curly_brace_end();

    sb.to_string()
  }
}

impl Node for NamedExports {
  fn kind(&self) -> Kind {
    Kind::NamedExports
  }
}
