use crate::{
  Text,
  Node,
  Kind,
  utils::StringBuilder,
};

use super::{
  NamedExports,
};

pub struct ExportDeclaration {
  export_clause: NamedExports,
}

impl ExportDeclaration {
  /// Creates new ExportDeclaration
  ///
  /// ```rust
  /// use tskit::export::{ ExportDeclaration, NamedExports, ExportSpecifier };
  /// use tskit::{ Text, Node, Kind };
  ///
  /// let mut nya = ExportSpecifier::new("nya");
  /// nya.prop_name("_nya");
  ///
  /// let elements = vec![nya, ExportSpecifier::new("b")];
  /// let named_exports = NamedExports::new(elements);
  /// let ex = ExportDeclaration::new(named_exports);
  ///
  /// assert_eq!(ex.text(), "export { _nya as nya, b }");
  /// assert_eq!(ex.kind(), Kind::ExportDeclaration);
  /// ```
  pub fn new(export_clause: NamedExports) -> Self {
    Self {
      export_clause,
    }
  }
}

impl Text for ExportDeclaration {
  fn text(&self) -> String {
    let mut sb = StringBuilder::with_capacity(40);

    sb
      .export()
      .space()
      .add(&self.export_clause.text());

    sb.to_string()
  }
}

impl Node for ExportDeclaration {
  fn kind(&self) -> Kind {
    Kind::ExportDeclaration
  }
}
