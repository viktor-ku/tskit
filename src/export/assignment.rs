use crate::{
  Text,
  Node,
  Kind,
  utils::StringBuilder,
};

pub struct ExportAssignment<'a> {
  expression: &'a Text,
}

impl<'a> ExportAssignment<'a> {
  /// Creates new ExportAssignment with the given expression
  ///
  /// ```rust
  /// use tskit::{ Node, Kind, Text };
  /// use tskit::export::ExportAssignment;
  /// use tskit::literal::NumericLiteral;
  ///
  /// let n: NumericLiteral = 42.into();
  /// let ex = ExportAssignment::new(&n);
  ///
  /// assert_eq!(ex.text(), "export default 42");
  /// assert_eq!(ex.kind(), Kind::ExportAssignment);
  /// ```
  pub fn new(expression: &'a Text) -> Self {
    Self {
      expression,
    }
  }
}

impl<'a> Text for ExportAssignment<'a> {
  fn text(&self) -> String {
    let mut sb = StringBuilder::with_capacity(40);

    sb.export()
      .space()
      .default()
      .space()
      .add(&self.expression.text());

    sb.to_string()
  }
}

impl<'a> Node for ExportAssignment<'a> {
  fn kind(&self) -> Kind {
    Kind::ExportAssignment
  }
}
