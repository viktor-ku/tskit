use crate::{
  Text,
  Node,
  Kind,
  utils::StringBuilder,
};

#[derive(Default)]
pub struct ReturnStatement<'a> {
  expression: Option<&'a Text>,
}

impl<'a> ReturnStatement<'a> {
  pub fn new() -> Self {
    Default::default()
  }

  pub fn with_expression(expression: &'a Text) -> Self {
    Self {
      expression: Some(expression),
    }
  }

  pub fn expression(&mut self, expression: &'a Text) -> &mut Self {
    self.expression = Some(expression);
    self
  }

  pub fn clear(&mut self) -> &mut Self {
    self.expression = None;
    self
  }
}

impl<'a> Text for ReturnStatement<'a> {
  fn text(&self) -> String {
    let mut sb = StringBuilder::with_capacity(100);

    sb.add_return();

    if let Some(expression) = &self.expression {
      sb
        .space()
        .add(&expression.text());
    }

    sb.to_string()
  }
}

impl<'a> Node for ReturnStatement<'a> {
  fn kind(&self) -> Kind {
    Kind::ReturnStatement
  }
}
