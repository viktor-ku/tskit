use crate::constants::*;

pub struct StringBuilder {
  val: String,
}

static INDENT_SIZE: &u8 = &2;

impl StringBuilder {
  pub fn with_capacity(capacity: usize) -> Self {
    Self {
      val: String::with_capacity(capacity),
    }
  }

  pub fn add(&mut self, val: &str) -> &mut Self {
    self.val.push_str(&val);
    self
  }

  pub fn remove_last(&mut self, n: usize) -> &mut Self {
    self.val.truncate(self.val.len() - n);
    self
  }

  pub fn asterisk(&mut self) -> &mut Self {
    self.val.push(ASTERISK);
    self
  }

  pub fn class(&mut self) -> &mut Self {
    self.val.push_str(CLASS);
    self
  }

  pub fn question(&mut self) -> &mut Self {
    self.val.push(QUESTION);
    self
  }

  pub fn at(&mut self) -> &mut Self {
    self.val.push(AT);
    self
  }

  pub fn from(&mut self) -> &mut Self {
    self.val.push_str(FROM);
    self
  }

  pub fn import(&mut self) -> &mut Self {
    self.val.push_str(IMPORT);
    self
  }

  pub fn export(&mut self) -> &mut Self {
    self.val.push_str(EXPORT);
    self
  }

  pub fn dot(&mut self) -> &mut Self {
    self.val.push(DOT);
    self
  }

  pub fn add_return(&mut self) -> &mut Self {
    self.val.push_str(RETURN);
    self
  }

  pub fn curly_brace_start(&mut self) -> &mut Self {
    self.val.push(CURLY_BRACE_START);
    self
  }
  pub fn curly_brace_end(&mut self) -> &mut Self {
    self.val.push(CURLY_BRACE_END);
    self
  }

  pub fn bracket_start(&mut self) -> &mut Self {
    self.val.push(BRACKET_START);
    self
  }
  pub fn bracket_end(&mut self) -> &mut Self {
    self.val.push(BRACKET_END);
    self
  }

  pub fn paren_start(&mut self) -> &mut Self {
    self.val.push(PAREN_START);
    self
  }
  pub fn paren_end(&mut self) -> &mut Self {
    self.val.push(PAREN_END);
    self
  }

  pub fn add_as(&mut self) -> &mut Self {
    self.val.push_str(AS);
    self
  }

  pub fn default(&mut self) -> &mut Self {
    self.val.push_str(DEFAULT);
    self
  }

  pub fn pipe(&mut self) -> &mut Self {
    self.val.push(PIPE);
    self
  }

  pub fn colon(&mut self) -> &mut Self {
    self.val.push(COLON);
    self
  }

  pub fn comma(&mut self) -> &mut Self {
    self.val.push(COMMA);
    self
  }

  pub fn space(&mut self) -> &mut Self {
    self.val.push(SPACE);
    self
  }

  pub fn pad(&mut self, pad_multiplier: u8) -> &mut Self {
    let mut cur = 0;
    let n = pad_multiplier * *INDENT_SIZE;

    while cur < n {
      self.val.push(SPACE);
      cur += 1;
    }

    self
  }

  pub fn equal(&mut self) -> &mut Self {
    self.val.push(EQ);
    self
  }

  pub fn eol(&mut self) -> &mut Self {
    self.val.push(EOL);
    self
  }

  pub fn to_string(&self) -> String {
    self.val.clone()
  }
}
