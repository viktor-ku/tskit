use super::{
  Kind,
  Node,
  Text,
  utils::StringBuilder,
  Identifier,
  Keyword,
};

pub struct PropertyDeclaration<'a> {
  name: Box<Identifier>,
  initializer: Option<&'a Text>,
  modifiers: Vec<&'a Text>,
  ts_type: Option<&'a Text>,
  question_token: bool,
}

impl<'a> PropertyDeclaration<'a> {
  pub fn new(text: &str) -> Self {
    Self {
      name: Identifier::new(text),
      initializer: None,
      modifiers: Vec::new(),
      ts_type: None,
      question_token: false,
    }
  }

  pub fn question(&mut self) -> &mut Self {
    self.question_token = true;
    self
  }

  pub fn ts_type(&mut self, val: &'a Text) -> &mut Self {
    self.ts_type = Some(val);
    self
  }

  pub fn init(&mut self, val: &'a Text) -> &mut Self {
    self.initializer = Some(val);
    self
  }

  pub fn modifier(&mut self, val: &'a Text) -> &mut Self {
    self.modifiers.push(val);
    self
  }

  /// Shortcut for adding the PublicKeyword modifier to the modifiers list
  pub fn public(&mut self) -> &mut Self {
    self.modifiers.push(&Keyword::Public);
    self
  }

  /// Shortcut for adding the PrivateKeyword modifier to the modifiers list
  pub fn private(&mut self) -> &mut Self {
    self.modifiers.push(&Keyword::Private);
    self
  }

  /// Shortcut for adding the ProtectedKeyword modifier to the modifiers list
  pub fn protected(&mut self) -> &mut Self {
    self.modifiers.push(&Keyword::Protected);
    self
  }
}

impl<'a> Text for PropertyDeclaration<'a> {
  fn text(&self) -> String {
    let mut sb = StringBuilder::with_capacity(20);

    if !self.modifiers.is_empty() {
      for modifier in &self.modifiers {
        sb
          .add(&modifier.text())
          .space();
      }
    }

    sb.add(&self.name.text());

    if self.question_token {
      sb.question();
    }

    if let Some(ts_type) = &self.ts_type {
      sb
        .colon()
        .space()
        .add(&ts_type.text());
    }

    if let Some(init) = &self.initializer {
      sb
        .space()
        .equal()
        .space()
        .add(&init.text());
    }

    sb.to_string()
  }
}

impl<'a> Node for PropertyDeclaration<'a> {
  fn kind(&self) -> Kind {
    Kind::PropertyDeclaration
  }
}
