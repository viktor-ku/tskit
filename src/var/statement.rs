use super::declaration_list::VarDeclarationList;

use crate::{
  Node,
  Text,
  Kind,
  keyword::Keyword,
  utils::StringBuilder,
};

pub struct VarStatement<'a> {
  declaration_list: VarDeclarationList<'a>,
  modifiers: Vec<&'a Text>,
}

impl<'a> VarStatement <'a> {
  pub fn new(declaration_list: VarDeclarationList<'a>) -> Self {
    Self {
      declaration_list,
      modifiers: Vec::new(),
    }
  }

  pub fn export(&mut self) -> &mut Self {
    self.modifiers.push(&Keyword::Export);
    self
  }
}

impl<'a> Node for VarStatement<'a> {
  fn kind(&self) -> Kind {
    Kind::VarStatement
  }
}

impl<'a> Text for VarStatement<'a> {
  fn text(&self) -> String {
    let mut sb = StringBuilder::with_capacity(10);

    if !self.modifiers.is_empty() {
      // TODO: assuming I have export modifier only for now
      sb
        .add(&self.modifiers.first().unwrap().text())
        .space();
    }

    sb.add(&self.declaration_list.text());

    sb.to_string()
  }
}
