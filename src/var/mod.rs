mod statement;
mod declaration_list;
mod declaration;

pub use self::{
  statement::VarStatement,
  declaration_list::VarDeclarationList,
  declaration::VarDeclaration,
};

use super::{
  Text,
  constants::{
    VAR,
    CONST,
    LET,
  },
};

/// ```rust
/// #[macro_use]
/// use tskit::var;
/// use tskit::var::*;
/// use tskit::Text;
/// use tskit::literal::StringLiteral;
/// use tskit::Keyword;
///
/// let name = StringLiteral::new("Viktor");
/// let string_t = Keyword::String;
///
/// assert_eq!(var!("name").text(), "const name");
/// assert_eq!(var!("name", &name).text(), "const name = 'Viktor'");
/// assert_eq!(var!("name", &string_t, &name).text(), "const name: string = 'Viktor'");
/// ```
#[macro_export]
macro_rules! var {
  ($name: expr) => {{
    let mut declaration_list = VarDeclarationList::new();
    let mut declaration = VarDeclaration::new($name);

    declaration_list.declaration(declaration);

    VarStatement::new(declaration_list)
  }};
  ($name: expr, $init: expr) => {{
    let mut declaration_list = VarDeclarationList::new();
    let mut declaration = VarDeclaration::new($name);
    declaration.init($init);

    declaration_list.declaration(declaration);

    VarStatement::new(declaration_list)
  }};
  ($name: expr, $type: expr, $init: expr) => {{
    let mut declaration_list = VarDeclarationList::new();
    let mut declaration = VarDeclaration::new($name);

    declaration
      .init($init)
      .ts_type($type);

    declaration_list.declaration(declaration);

    VarStatement::new(declaration_list)
  }};
}

/// Defines variable kind
#[derive(Clone)]
pub enum VarKind {
  /// var
  Var,

  /// let
  Let,

  /// const
  Const,
}

impl Default for VarKind {
  fn default() -> Self {
    VarKind::Const
  }
}

impl Text for VarKind {
  fn text(&self) -> String {
    match self {
      VarKind::Var => VAR.to_string(),
      VarKind::Const => CONST.to_string(),
      VarKind::Let => LET.to_string(),
    }
  }
}
