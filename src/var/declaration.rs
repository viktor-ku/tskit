use crate::{
  Identifier,
  Kind,
  Text,
  Node,
  utils::StringBuilder,
};

pub struct VarDeclaration<'a> {
  name: Box<Identifier>,
  initializer: Option<&'a Text>,
  ts_type: Option<&'a Text>,
}

impl<'a> VarDeclaration<'a> {
  pub fn new(name: &'a str) -> Self {
    Self {
      name: Identifier::new(name),
      initializer: None,
      ts_type: None,
    }
  }

  pub fn init(&mut self, initializer: &'a Text) -> &mut Self {
    self.initializer = Some(initializer);
    self
  }

  pub fn ts_type(&mut self, val: &'a Text) -> &mut Self {
    self.ts_type = Some(val);
    self
  }
}

impl<'a> Node for VarDeclaration<'a> {
  fn kind(&self) -> Kind {
    Kind::VarDeclaration
  }
}

impl<'a> Text for VarDeclaration<'a> {
  fn text(&self) -> String {
    let mut sb = StringBuilder::with_capacity(10);

    sb.add(&self.name.text());

    if self.ts_type.is_some() {
      sb.colon()
        .space()
        .add(&self.ts_type.unwrap().text());
    }

    if self.initializer.is_some() {
      sb.space()
        .equal()
        .space()
        .add(&self.initializer.unwrap().text());
    }

    sb.to_string()
  }
}
