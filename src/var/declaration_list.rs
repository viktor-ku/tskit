use super::declaration::VarDeclaration;
use super::VarKind;

use crate::{
  Node,
  Kind,
  Text,
  utils::StringBuilder,
};

#[derive(Default)]
pub struct VarDeclarationList<'a> {
  declarations: Vec<VarDeclaration<'a>>,
  kind: VarKind,
}

impl<'a> VarDeclarationList<'a> {
  pub fn new() -> Self {
    Default::default()
  }

  pub fn declaration(&mut self, decl: VarDeclaration<'a>) -> &mut Self {
    self.declarations.push(decl);
    self
  }

  pub fn let_mod(&mut self) -> &mut Self {
    self.kind = VarKind::Let;
    self
  }

  pub fn var_mod(&mut self) -> &mut Self {
    self.kind = VarKind::Var;
    self
  }
}

impl<'a> Node for VarDeclarationList<'a> {
  fn kind(&self) -> Kind {
    Kind::VarDeclarationList
  }
}

impl<'a> Text for VarDeclarationList<'a> {
  fn text(&self) -> String {
    let mut sb = StringBuilder::with_capacity(10);

    sb.add(&self.kind.text())
      .space();

    for decl in self.declarations.iter() {
      sb.add(&decl.text())
        .comma()
        .space();
    }

    sb.remove_last(2);

    sb.to_string()
  }
}
