use super::{
  Kind,
  Node,
  Text,
  utils::StringBuilder,
  Identifier,
};

pub struct PropertyAssignment<'a> {
  name: Box<Identifier>,
  initializer: &'a Text,
}

impl<'a> PropertyAssignment<'a> {
  /// ```rust
  /// # use tskit::PropertyAssignment;
  /// # use tskit::literal::NumericLiteral;
  /// # use tskit::{ Node, Kind, Text };
  ///
  /// let one: NumericLiteral = 1.into();
  /// let prop_assignment = PropertyAssignment::new("a", &one);
  ///
  /// assert_eq!(prop_assignment.text(), "a: 1");
  /// assert_eq!(prop_assignment.kind(), Kind::PropertyAssignment);
  /// ```
  pub fn new(text: &str, initializer: &'a Text) -> Self {
    Self {
      name: Identifier::new(text),
      initializer,
    }
  }
}

impl<'a> Text for PropertyAssignment<'a> {
  fn text(&self) -> String {
    let mut sb = StringBuilder::with_capacity(20);

    sb
      .add(&self.name.text())
      .colon()
      .space()
      .add(&self.initializer.text());

    sb.to_string()
  }
}

impl<'a> Node for PropertyAssignment<'a> {
  fn kind(&self) -> Kind {
    Kind::PropertyAssignment
  }
}
