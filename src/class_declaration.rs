use crate::{
  Text,
  Node,
  Kind,
  utils::StringBuilder,
  Identifier,
  Decorator,
  HeritageClause,
  ExpressionWithTypeArguments,
  Token,
};

#[derive(Default)]
pub struct ClassDeclaration<'a> {
  name: Option<Box<Identifier>>,
  members: Vec<&'a Text>,
  decorators: Vec<&'a Decorator<'a>>,
  heritage_clauses: Vec<HeritageClause<'a>>,
}

impl<'a> ClassDeclaration<'a> {
  pub fn new() -> Self {
    Default::default()
  }

  pub fn with_capacity(capacity: usize) -> Self {
    Self {
      name: None,
      members: Vec::with_capacity(capacity),
      decorators: Vec::new(),
      heritage_clauses: Vec::new(),
    }
  }

  pub fn heritage_clause(&mut self, val: HeritageClause<'a>) -> &mut Self {
    self.heritage_clauses.push(val);
    self
  }

  pub fn decorator(&mut self, val: &'a Decorator<'a>) -> &mut Self {
    self.decorators.push(val);
    self
  }

  pub fn name(&mut self, val: &str) -> &mut Self {
    self.name = Some(Identifier::new(val));
    self
  }

  pub fn member(&mut self, val: &'a Text) -> &mut Self {
    self.members.push(val);
    self
  }
}

impl<'a> Text for ClassDeclaration<'a> {
  fn text(&self) -> String {
    let mut sb = StringBuilder::with_capacity(100);

    if !self.decorators.is_empty() {
      for decorator in &self.decorators {
        sb
          .add(&decorator.text())
          .eol();
      }
    }

    sb
      .class()
      .space();

    if let Some(name) = &self.name {
      sb
        .add(&name.text())
        .space();
    }

    if !self.heritage_clauses.is_empty() {
      for heritage_clause in &self.heritage_clauses {
        sb.add(&heritage_clause.text());
      }
    }

    sb.curly_brace_start();

    if !self.members.is_empty() {
      sb.eol();

      for member in &self.members {
        sb
          .pad(1)
          .add(&member.text())
          .eol();
      }

      sb
        .remove_last(1)
        .eol();
    }

    sb.curly_brace_end();

    sb.to_string()
  }
}

impl<'a> Node for ClassDeclaration<'a> {
  fn kind(&self) -> Kind {
    Kind::ClassDeclaration
  }
}
