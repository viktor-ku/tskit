use super::{
  Kind,
  Node,
  Text,
  utils::StringBuilder,
};

pub struct CallExpression<'a> {
  expression: &'a Text,
  arguments: Vec<&'a Text>,
}

impl<'a> CallExpression<'a> {
  pub fn new(expression: &'a Text) -> Self {
    Self {
      expression,
      arguments: Vec::new(),
    }
  }

  pub fn argument(&mut self, val: &'a Text) -> &mut Self {
    self.arguments.push(val);
    self
  }
}

impl<'a> Text for CallExpression<'a> {
  fn text(&self) -> String {
    let mut sb = StringBuilder::with_capacity(40);

    sb.add(&self.expression.text());

    sb.paren_start();

    if !self.arguments.is_empty() {
      for argument in &self.arguments {
        sb
          .add(&argument.text())
          .comma()
          .space();
      }

      sb.remove_last(2);
    }

    sb.paren_end();

    sb.to_string()
  }
}

impl<'a> Node for CallExpression<'a> {
  fn kind(&self) -> Kind {
    Kind::CallExpression
  }
}
