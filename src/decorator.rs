use super::{
  Kind,
  Node,
  Text,
  utils::StringBuilder,
};

pub struct Decorator<'a> {
  expression: &'a Text,
}

impl<'a> Decorator<'a> {
  pub fn new(expression: &'a Text) -> Self {
    Self {
      expression,
    }
  }
}

impl<'a> Text for Decorator<'a> {
  fn text(&self) -> String {
    let mut sb = StringBuilder::with_capacity(40);

    sb
      .at()
      .add(&self.expression.text());

    sb.to_string()
  }
}

impl<'a> Node for Decorator<'a> {
  fn kind(&self) -> Kind {
    Kind::Decorator
  }
}
