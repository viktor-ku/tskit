use super::{
  Text,
  Node,
  kind::Kind,
  constants::*,
};

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Keyword {
  True,
  False,
  Null,
  Undefined,
  NaN,
  Async,
  Boolean,
  Export,
  Import,
  Number,
  Private,
  Protected,
  Public,
  Static,
  String,
  This,
}

impl Text for Keyword {
  fn text(&self) -> String {
    match self {
      Keyword::True => String::from(TRUE),
      Keyword::False => String::from(FALSE),
      Keyword::Null => String::from(NULL),
      Keyword::Undefined => String::from(UNDEFINED),
      Keyword::NaN => String::from(NAN),
      Keyword::Async => String::from(ASYNC),
      Keyword::Boolean => String::from(BOOLEAN),
      Keyword::Export => String::from(EXPORT),
      Keyword::Import => String::from(IMPORT),
      Keyword::Number => String::from(NUMBER),
      Keyword::Private => String::from(PRIVATE),
      Keyword::Protected => String::from(PROTECTED),
      Keyword::Public => String::from(PUBLIC),
      Keyword::Static => String::from(STATIC),
      Keyword::String => String::from(STRING),
      Keyword::This => String::from(THIS),
    }
  }
}

impl Node for Keyword {
  fn kind(&self) -> Kind {
    match self {
      Keyword::True => Kind::TrueKeyword,
      Keyword::False => Kind::FalseKeyword,
      Keyword::Null => Kind::NullKeyword,
      Keyword::Undefined => Kind::UndefinedKeyword,
      Keyword::NaN => Kind::NaNKeyword,
      Keyword::Async => Kind::AsyncKeyword,
      Keyword::Boolean => Kind::BooleanKeyword,
      Keyword::Export => Kind::ExportKeyword,
      Keyword::Import => Kind::ImportKeyword,
      Keyword::Number => Kind::NumberKeyword,
      Keyword::Private => Kind::PrivateKeyword,
      Keyword::Protected => Kind::ProtectedKeyword,
      Keyword::Public => Kind::PublicKeyword,
      Keyword::Static => Kind::StaticKeyword,
      Keyword::String => Kind::StringKeyword,
      Keyword::This => Kind::ThisKeyword,
    }
  }
}
