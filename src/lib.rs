pub trait Node {
  fn kind(&self) -> Kind;
}

pub trait Text {
  fn text(&self) -> String;
}

pub mod var;
pub mod literal;
pub mod utils;
pub mod constants;
pub mod export;
pub mod import;

mod expression_with_type_arguments;
pub use self::expression_with_type_arguments::ExpressionWithTypeArguments;

mod heritage_clause;
pub use self::heritage_clause::HeritageClause;

mod call_expression;
pub use self::call_expression::CallExpression;

mod decorator;
pub use self::decorator::Decorator;

mod class_declaration;
pub use self::class_declaration::ClassDeclaration;

mod property_declaration;
pub use self::property_declaration::PropertyDeclaration;

mod property_access_expression;
pub use self::property_access_expression::PropertyAccessExpression;

mod return_statement;
pub use self::return_statement::ReturnStatement;

mod property_assignment;
pub use self::property_assignment::PropertyAssignment;

mod method_declaration;
pub use self::method_declaration::MethodDeclaration;

mod block;
pub use self::block::Block;

mod token;
pub use self::token::Token;

mod union_type;
pub use self::union_type::UnionType;

mod keyword;
pub use self::keyword::Keyword;

mod identifier;
pub use self::identifier::Identifier;

mod kind;
pub use self::kind::Kind;
