use super::{
  Kind,
  Node,
  Text,
};

#[derive(Default)]
pub struct Identifier {
  text: String,
}

impl Identifier {
  /// Creates new Identifier object
  ///
  /// ```rust
  /// # use tskit::Identifier;
  /// # use tskit::{ Node, Kind };
  ///
  /// let id = Identifier::new("someId");
  /// assert_eq!(id.kind(), Kind::Identifier);
  /// ```
  pub fn new(text: &str) -> Box<Self> {
    Box::new(Self {
      text: text.to_string(),
    })
  }
}

impl Text for Identifier {
  fn text(&self) -> String {
    self.text.clone()
  }
}

impl Node for Identifier {
  fn kind(&self) -> Kind {
    Kind::Identifier
  }
}
