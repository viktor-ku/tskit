pub static EQ: char = '=';
pub static SPACE: char = ' ';
pub static COLON: char = ':';
pub static SEMICOLON: char = ';';
pub static PIPE: char = '|';
pub static COMMA: char = ',';
pub static EOL: char = '\n';
pub static AT: char = '@';

pub static VAR: &'static str = "var";
pub static CONST: &'static str = "const";
pub static LET: &'static str = "let";

pub static CLASS: &'static str = "class";
pub static TRUE: &'static str = "true";
pub static FALSE: &'static str = "false";
pub static NULL: &'static str = "null";
pub static UNDEFINED: &'static str = "undefined";
pub static NAN: &'static str = "NaN";
pub static ASYNC: &'static str = "async";
pub static EXPORT: &'static str = "export";
pub static DEFAULT: &'static str = "default";
pub static IMPORT: &'static str = "import";
pub static THIS: &'static str = "this";
pub static AS: &'static str = "as";
pub static FROM: &'static str = "from";
pub static RETURN: &'static str = "return";
pub static EXTENDS: &'static str = "extends";
pub static IMPLEMENTS: &'static str = "implements";

pub static CURLY_BRACE_START: char = '{';
pub static CURLY_BRACE_END: char = '}';

pub static BRACKET_START: char = '[';
pub static BRACKET_END: char = ']';

pub static PAREN_START: char = '(';
pub static PAREN_END: char = ')';

pub static AMP_AMP: &'static str = "&&";
pub static AMP: char = '&';
pub static ASTERISK: char = '*';
pub static BAR_BAR: &'static str = "||";
pub static DOT: char = '.';
pub static DOT_DOT_DOT: &'static str = "...";
pub static EQUALS_GREATER_THAN: &'static str = "=>";
pub static EXCLAMATION: char = '!';
pub static MINUS: char = '-';
pub static PLUS: char = '+';
pub static QUESTION: char = '?';

pub static NUMBER: &'static str = "number";
pub static PRIVATE: &'static str = "private";
pub static PROTECTED: &'static str = "protected";
pub static PUBLIC: &'static str = "public";
pub static STATIC: &'static str = "static";
pub static STRING: &'static str = "string";
pub static BOOLEAN: &'static str = "boolean";
