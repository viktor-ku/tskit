use crate::{
  kind::Kind,
  Text,
  Node,
  utils::StringBuilder,
};

#[derive(Default)]
pub struct UnionType<'a> {
  types: Vec<&'a Text>,
}

impl<'a> UnionType<'a> {
  pub fn new() -> Self {
    Default::default()
  }

  pub fn add(&mut self, node: &'a Text) -> &mut Self {
    self.types.push(node);
    self
  }
}

impl<'a> Text for UnionType<'a> {
  fn text(&self) -> String {
    let mut sb = StringBuilder::with_capacity(10);

    for t in self.types.iter() {
      sb.add(&t.text())
        .space()
        .pipe()
        .space();
    }

    sb.remove_last(3);

    sb.to_string()
  }
}

impl<'a> Node for UnionType<'a> {
  fn kind(&self) -> Kind {
    Kind::UnionType
  }
}
