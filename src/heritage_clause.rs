use super::{
  Kind,
  Node,
  Text,
  utils::StringBuilder,
  Token,
};

pub struct HeritageClause<'a> {
  token: Token,
  types: Vec<&'a Text>,
}

impl<'a> HeritageClause<'a> {
  pub fn new(token: Token, val: &'a Text) -> Self {
    Self {
      token,
      types: vec![val],
    }
  }
}

impl<'a> Text for HeritageClause<'a> {
  fn text(&self) -> String {
    let mut sb = StringBuilder::with_capacity(40);

    sb
      .add(&self.token.text())
      .space();

    for t in &self.types {
      sb.add(&t.text());
    }

    sb.space();

    sb.to_string()
  }
}

impl<'a> Node for HeritageClause<'a> {
  fn kind(&self) -> Kind {
    Kind::HeritageClause
  }
}
