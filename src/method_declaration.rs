use crate::{
  Identifier,
  Text,
  Node,
  Kind,
  utils::StringBuilder,
  Block,
};

pub struct MethodDeclaration<'a> {
  name: Box<Identifier>,
  body: Block<'a>,
}

impl<'a> MethodDeclaration<'a> {
  pub fn new(name: &str) -> Self {
    Self {
      name: Identifier::new(name),
      body: Block::new(),
    }
  }

  pub fn body(&mut self, block: Block<'a>) -> &mut Self {
    self.body = block;
    self
  }
}

impl<'a> Text for MethodDeclaration<'a> {
  fn text(&self) -> String {
    let mut sb = StringBuilder::with_capacity(100);

    sb.add(&self.name.text());

    sb.paren_start();
    sb.paren_end();

    sb.space();

    sb.add(&self.body.text());

    sb.to_string()
  }
}

impl<'a> Node for MethodDeclaration<'a> {
  fn kind(&self) -> Kind {
    Kind::MethodDeclaration
  }
}
