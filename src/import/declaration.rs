use crate::{
  Text,
  Node,
  Kind,
  utils::StringBuilder,
  literal::StringLiteral,
};

use super::{
  ImportClause,
};

pub struct ImportDeclaration<'a> {
  import_clause: ImportClause<'a>,
  module_specifier: StringLiteral,
}

impl<'a> ImportDeclaration<'a> {
  /// ```rust
  /// use tskit::import::{ ImportDeclaration, NamedImports, ImportSpecifier, ImportClause };
  /// use tskit::{ Text, Node, Kind };
  ///
  /// let mut nya = ImportSpecifier::new("nya");
  /// nya.prop_name("_nya");
  ///
  /// let elements = vec![nya, ImportSpecifier::new("b")];
  /// let named = NamedImports::new(elements);
  /// let mut clause = ImportClause::new();
  /// clause.named_bindings(&named);
  /// let im = ImportDeclaration::new(clause, "nya");
  ///
  /// assert_eq!(im.text(), "import { _nya as nya, b } from 'nya'");
  /// assert_eq!(im.kind(), Kind::ImportDeclaration);
  /// ```
  pub fn new(import_clause: ImportClause<'a>, module_specifier: &str) -> Self {
    Self {
      import_clause,
      module_specifier: StringLiteral::new(module_specifier),
    }
  }
}

impl<'a> Text for ImportDeclaration<'a> {
  fn text(&self) -> String {
    let mut sb = StringBuilder::with_capacity(40);

    sb
      .import()
      .space()
      .add(&self.import_clause.text())
      .space()
      .from()
      .space()
      .add(&self.module_specifier.text());

    sb.to_string()
  }
}

impl<'a> Node for ImportDeclaration<'a> {
  fn kind(&self) -> Kind {
    Kind::ImportDeclaration
  }
}
