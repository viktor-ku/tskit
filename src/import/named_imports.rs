use crate::{
  Text,
  Node,
  Kind,
  utils::StringBuilder,
};

use super::{
  ImportSpecifier,
};

pub struct NamedImports {
  elements: Vec<ImportSpecifier>,
}

impl NamedImports {
  /// ```rust
  /// use tskit::import::{ NamedImports, ImportSpecifier };
  /// use tskit::{ Text, Node, Kind };
  ///
  /// let elements = vec![ImportSpecifier::new("a"), ImportSpecifier::new("b")];
  /// let named_im = NamedImports::new(elements);
  ///
  /// assert_eq!(named_im.text(), "{ a, b }");
  /// assert_eq!(named_im.kind(), Kind::NamedImports);
  /// ```
  pub fn new(elements: Vec<ImportSpecifier>) -> Self {
    Self {
      elements,
    }
  }

  /// Creates new NamedImports with given capacity in case we know exact number of imports in
  /// advance.
  ///
  /// ```rust
  /// use tskit::import::{ NamedImports, ImportSpecifier };
  /// use tskit::{ Text, Node };
  ///
  /// let elements = vec![ImportSpecifier::new("a"), ImportSpecifier::new("b")];
  /// let mut named_im = NamedImports::with_capacity(elements.len());
  ///
  /// named_im
  ///   .elements(elements);
  ///
  /// assert_eq!(named_im.text(), "{ a, b }");
  /// ```
  pub fn with_capacity(capacity: usize) -> Self {
    Self {
      elements: Vec::with_capacity(capacity),
    }
  }

  /// Use to reset ImportSpecifiers vector
  pub fn elements(&mut self, val: Vec<ImportSpecifier>) -> &mut Self {
    self.elements = val;
    self
  }

  /// Use to add ImportSpecifier to the elements vector
  ///
  /// ```rust
  /// use tskit::import::{ NamedImports, ImportSpecifier };
  /// use tskit::{ Text, Node };
  ///
  /// let mut named_im = NamedImports::with_capacity(2);
  ///
  /// named_im
  ///   .element(ImportSpecifier::new("a"))
  ///   .element(ImportSpecifier::new("b"));
  ///
  /// assert_eq!(named_im.text(), "{ a, b }");
  /// ```
  pub fn element(&mut self, val: ImportSpecifier) -> &mut Self {
    self.elements.push(val);
    self
  }
}

impl Text for NamedImports {
  fn text(&self) -> String {
    let mut sb = StringBuilder::with_capacity(40);

    sb.curly_brace_start()
      .space();

    for el in &self.elements {
      sb.add(&el.text())
        .comma()
        .space();
    }

    sb.remove_last(2) // removes space + comma from the last elements iteration
      .space()
      .curly_brace_end();

    sb.to_string()
  }
}

impl Node for NamedImports {
  fn kind(&self) -> Kind {
    Kind::NamedImports
  }
}
