use crate::{
  Text,
  Node,
  Kind,
  utils::StringBuilder,
  Identifier,
};

pub struct ImportSpecifier {
  name: Box<Identifier>,
  property_name: Option<Box<Identifier>>,
}

impl ImportSpecifier {
  /// Creates new ImportSpecifier
  ///
  /// ```rust
  /// use tskit::{ Kind, Node, Text };
  /// use tskit::import::ImportSpecifier;
  ///
  /// let im = ImportSpecifier::new("a");
  ///
  /// assert_eq!(im.text(), "a");
  /// assert_eq!(im.kind(), Kind::ImportSpecifier);
  /// ```
  pub fn new(name: &str) -> Self {
    Self {
      name: Identifier::new(name),
      property_name: None,
    }
  }

  /// Add propertyName identifier
  ///
  /// ```rust
  /// use tskit::{ Kind, Node, Text };
  /// use tskit::import::ImportSpecifier;
  ///
  /// let mut im_as = ImportSpecifier::new("b");
  /// im_as.prop_name("a");
  ///
  /// assert_eq!(im_as.text(), "a as b");
  /// ```
  pub fn prop_name(&mut self, val: &str) -> &mut Self {
    self.property_name = Some(Identifier::new(val));
    self
  }
}

impl Text for ImportSpecifier {
  fn text(&self) -> String {
    let mut sb = StringBuilder::with_capacity(20);

    if let Some(id) = &self.property_name {
      sb.add(&id.text())
        .space()
        .add_as()
        .space();
    }

    sb.add(&self.name.text());

    sb.to_string()
  }
}

impl Node for ImportSpecifier {
  fn kind(&self) -> Kind {
    Kind::ImportSpecifier
  }
}
