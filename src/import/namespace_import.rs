use crate::{
  Text,
  Node,
  Kind,
  Identifier,
  utils::StringBuilder,
};

pub struct NamespaceImport {
  name: Box<Identifier>,
}

impl NamespaceImport {
  /// ```rust
  /// use tskit::{ Kind, Node, Text };
  /// use tskit::import::NamespaceImport;
  ///
  /// let ns = NamespaceImport::new("yay");
  ///
  /// assert_eq!(ns.text(), "* as yay");
  /// assert_eq!(ns.kind(), Kind::NamespaceImport);
  /// ```
  pub fn new(name: &str) -> Self {
    Self {
      name: Identifier::new(name),
    }
  }
}

impl Text for NamespaceImport {
  fn text(&self) -> String {
    let mut sb = StringBuilder::with_capacity(20);

    sb
      .asterisk()
      .space()
      .add_as()
      .space()
      .add(&self.name.text());

    sb.to_string()
  }
}

impl Node for NamespaceImport {
  fn kind(&self) -> Kind {
    Kind::NamespaceImport
  }
}
