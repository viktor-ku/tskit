use crate::{
  Text,
  Node,
  Kind,
  Identifier,
  utils::StringBuilder,
};

#[derive(Default)]
pub struct ImportClause<'a> {
  name: Option<Box<Identifier>>,
  named_bindings: Option<&'a Text>,
}

impl<'a> ImportClause<'a> {
  pub fn new() -> Self {
    Default::default()
  }

  pub fn name(&mut self, val: &str) -> &mut Self {
    self.name = Some(Identifier::new(val));
    self
  }

  pub fn named_bindings(&mut self, val: &'a Text) -> &mut Self {
    self.named_bindings = Some(val);
    self
  }
}

impl<'a> Text for ImportClause<'a> {
  fn text(&self) -> String {
    let mut sb = StringBuilder::with_capacity(20);

    let name_ok = self.name.is_some();
    let named_bindings_ok = self.named_bindings.is_some();

    if let Some(name) = &self.name {
      sb.add(&name.text());
    }

    if name_ok && named_bindings_ok {
      sb.comma()
        .space();
    }

    if let Some(named_bindings) = &self.named_bindings {
      sb.add(&named_bindings.text());
    }

    sb.to_string()
  }
}

impl<'a> Node for ImportClause<'a> {
  fn kind(&self) -> Kind {
    Kind::ImportClause
  }
}
