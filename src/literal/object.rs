use crate::{
  Text,
  Node,
  Kind,
  utils::StringBuilder,
};

#[derive(Default)]
pub struct ObjectLiteralExpression<'a> {
  properties: Vec<&'a Text>,
}

impl<'a> ObjectLiteralExpression<'a> {
  /// ```rust
  /// use tskit::{ Kind, Node, Text };
  /// use tskit::literal::ObjectLiteralExpression;
  ///
  /// let obj = ObjectLiteralExpression::new();
  /// assert_eq!(obj.text(), "{}");
  /// ```
  pub fn new() -> Self {
    Default::default()
  }

  pub fn prop(&mut self, val: &'a Text) -> &mut Self {
    self.properties.push(val);
    self
  }

  /// Funny that because properties vector contains anything implementing the Text trait, this
  /// method and the prop method are essentially equal and can be used for adding both
  /// MethodDeclarations and PropertyAssignments
  pub fn method(&mut self, val: &'a Text) -> &mut Self {
    self.properties.push(val);
    self
  }
}

impl<'a> Text for ObjectLiteralExpression<'a> {
  fn text(&self) -> String {
    let mut sb = StringBuilder::with_capacity(40);

    sb.curly_brace_start();

    if !self.properties.is_empty() {
      sb.eol();

      for prop in &self.properties {
        sb
          .pad(1)
          .add(&prop.text())
          .comma()
          .eol();
      }
    }

    sb.curly_brace_end();

    sb.to_string()
  }
}

impl<'a> Node for ObjectLiteralExpression<'a> {
  fn kind(&self) -> Kind {
    Kind::ObjectLiteralExpression
  }
}
