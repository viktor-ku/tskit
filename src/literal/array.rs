use crate::{
  Text,
  Node,
  Kind,
  utils::StringBuilder,
};

#[derive(Default)]
pub struct ArrayLiteralExpression<'a> {
  elements: Vec<&'a Text>,
}

impl<'a> ArrayLiteralExpression<'a> {
  /// Creates new ArrayLiteralExpression
  ///
  /// ```rust
  /// use tskit::{ Kind, Node, Text };
  /// use tskit::literal::ArrayLiteralExpression;
  /// use tskit::literal::{ NumericLiteral, StringLiteral };
  ///
  /// let a: NumericLiteral = 1.into();
  /// let b: NumericLiteral = 2.2.into();
  /// let foo: StringLiteral = "foo".into();
  ///
  /// let mut arr = ArrayLiteralExpression::new();
  ///
  /// arr.push(&a)
  ///    .push(&b)
  ///    .push(&foo);
  ///
  /// assert_eq!(arr.text(), "[1, 2.2, 'foo']");
  /// assert_eq!(arr.kind(), Kind::ArrayLiteralExpression);
  /// ```
  pub fn new() -> Self {
    Default::default()
  }

  pub fn with_capacity(capacity: usize) -> Self {
    Self {
      elements: Vec::with_capacity(capacity),
    }
  }

  pub fn push(&mut self, item: &'a Text) -> &mut Self {
    self.elements.push(item);
    self
  }
}

impl<'a> Text for ArrayLiteralExpression<'a> {
  fn text(&self) -> String {
    let mut sb = StringBuilder::with_capacity(20);

    sb.bracket_start();

    for el in &self.elements {
      sb
        .add(&el.text())
        .comma()
        .space();
    }

    sb
      .remove_last(2)
      .bracket_end();

    sb.to_string()
  }
}

impl<'a> Node for ArrayLiteralExpression<'a> {
  fn kind(&self) -> Kind {
    Kind::ArrayLiteralExpression
  }
}
