use crate::{
  Text,
  Node,
  Kind,
};

#[derive(Clone, Debug, PartialEq, PartialOrd)]
pub struct StringLiteral {
  text: String,
}

impl StringLiteral {
  /// Creates new String from f64
  ///
  /// ```rust
  /// use tskit::literal::StringLiteral;
  /// use tskit::{ Text, Node, Kind };
  ///
  /// let name = StringLiteral::new("Viktor");
  /// assert_eq!(name.text(), "'Viktor'");
  /// assert_eq!(name.kind(), Kind::StringLiteral);
  ///
  /// let or: StringLiteral = "Viktor".into();
  /// assert_eq!(or.text(), "'Viktor'");
  /// ```
  pub fn new(text: &str) -> Self {
    Self {
      text: text.to_string(),
    }
  }
}

impl Text for StringLiteral {
  fn text(&self) -> String {
    format!("'{}'", self.text)
  }
}

impl Node for StringLiteral {
  fn kind(&self) -> Kind {
    Kind::StringLiteral
  }
}

impl From<&str> for StringLiteral {
  fn from(text: &str) -> Self {
    Self::new(text)
  }
}
