use crate::{
  Text,
  Node,
  Kind,
};

#[derive(Clone, Debug, PartialEq, PartialOrd)]
pub struct NumericLiteral {
  number: f64,
}

impl NumericLiteral {
  /// Creates new NumericLiteral from f64
  ///
  /// ```rust
  /// use tskit::literal::NumericLiteral;
  /// use tskit::{ Text, Node, Kind };
  ///
  /// let age = NumericLiteral::new(23.0);
  /// assert_eq!(age.text(), "23");
  /// assert_eq!(age.kind(), Kind::NumericLiteral);
  ///
  /// let a: NumericLiteral = 1.0.into();
  /// let b: NumericLiteral = 1.into();
  /// assert_eq!(a.text(), b.text());
  /// ```
  pub fn new(number: f64) -> Self {
    Self {
      number,
    }
  }
}

impl Text for NumericLiteral {
  fn text(&self) -> String {
    self.number.to_string()
  }
}

impl Node for NumericLiteral {
  fn kind(&self) -> Kind {
    Kind::NumericLiteral
  }
}

impl From<f64> for NumericLiteral {
  fn from(number: f64) -> Self {
    Self::new(number)
  }
}

impl From<i64> for NumericLiteral {
  fn from(number: i64) -> Self {
    Self::new(number as f64)
  }
}
