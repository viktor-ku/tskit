# tskit

> TypeScript AST, code generation, parser in Rust

Work In Progress. In fact, I just started, but you already can describe a log of things and then generate valid TypeScript.

## Roadmap for v1.0.0

- [ ] Complete all AST elements
- [ ] Implement Parser
- [ ] Test coverage
- [ ] Add macros (right now there is only `var!` macro)
